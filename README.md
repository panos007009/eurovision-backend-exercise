# Backend part of the Sandbox Interview Exercise of Eurovision Labs

A Spring Boot app that utilises the given dockerized MariaDB to perform business logic on the data and return the modified results as API endpoints.

## How to run the app

The application is modified to expect the database in port 1112 (full datasource url is **jdbc:mariadb://localhost:1112/eurovisiondb** ). This can be modified in the application.properties file in the resources folder by changing `spring.datasource.url`. <br> It is also modified to run at port 1111. This can also be modified in the same file by changing `server.port`

1. Start the database container in docker at the appropriate port
2. Open the app in your favourite IDE
3. Run the app as a Spring Boot application
4. Test the endpoints either directly on your browser or with a specified tool like Postman

## Exposed endpoints

1. GET http://localhost:1111/api/cities <br> This endpoint returns all the contents of the cities table on ascending order by name for verification purposes
2. GET http://localhost:1111/api/longest-sequence-cities <br> This endpoint returns the result of the application of the first algorithm (Longest Sequence), on all the contents of the table cities.
3. GET http://localhost:1111/api/cities/queryByPage <br> This endpoint returns the contents of the table cities but with respect to the page and size parameters passed in the request and is intended to be used in the frontend part of the exercise. <br><br>
Example of response without any parameters returning the result using default values (page=0, size=5) <br><br>
  
        {
        "content": [
        {
        "id": 233,
        "name": "Abadan"
        },
        {
        "id": 67,
        "name": "Abidjan"
        },
        {
        "id": 157,
        "name": "Accra"
        },
        {
        "id": 262,
        "name": "Ad-Dammam"
        },
        {
        "id": 255,
        "name": "Adana"
        }
        ],
        "totalPages": 67,
        "totalElements": 332
        }
