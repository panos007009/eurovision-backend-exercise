package com.example.eurovisionbackendexercise.controller;

import com.example.eurovisionbackendexercise.dao.CityRepository;
import com.example.eurovisionbackendexercise.entity.City;
import com.example.eurovisionbackendexercise.service.ServiceCity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private ServiceCity serviceCity;

    @Autowired
    private CityRepository cityRepository;

    //endpoint to fetch all cities for verification purposes
    @CrossOrigin
    @GetMapping("/cities")
    public List<City> getCities() {
        return serviceCity.listAll();
    }

    //endpoint to fetch the longest possible sequence of city Id's which form an ascending list of numbers
    @GetMapping("/longest-sequence-cities")
    public List<City> getLongestSequenceCities() {
        return serviceCity.listLongestSequence();
    }

    //endpoint to provide Angular a pageable table of all cities with their ids
    @GetMapping("/cities/queryByPage")
    public ResponseEntity<Map<String,Object>> getCitiesPaged(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size
    ) {
        List<City> cities;

        //page request parameters deriving from the request made
        Pageable paging = PageRequest.of(page, size);

        Page<City> pageOfCities;

        //get the page of cities
        pageOfCities = cityRepository.findAll(paging);

        //convert the page to a list of cities to map them to the JSON response
        cities = pageOfCities.getContent();

        //using linked hash map so that the order of the elements in the response doesn't get mixed up
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("content", cities);
        response.put("totalPages", pageOfCities.getTotalPages());
        response.put("totalElements", pageOfCities.getTotalElements());

        //return the final JSON response
        return new ResponseEntity<>(response, HttpStatus.OK) ;
    }
}
