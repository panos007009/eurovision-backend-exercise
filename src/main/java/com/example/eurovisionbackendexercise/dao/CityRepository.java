package com.example.eurovisionbackendexercise.dao;

import com.example.eurovisionbackendexercise.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//repository for the cities table
@CrossOrigin
public interface CityRepository extends JpaRepository<City, Integer> {


}
