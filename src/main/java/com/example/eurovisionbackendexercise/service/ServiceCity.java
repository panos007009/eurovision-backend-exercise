package com.example.eurovisionbackendexercise.service;

import com.example.eurovisionbackendexercise.dao.CityRepository;
import com.example.eurovisionbackendexercise.entity.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceCity {

    @Autowired
    private CityRepository cityRepository;

    //service to fetch all cities for verification purposes
    public List<City> listAll() {
        return cityRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    //service to fetch all cities
    public List<City> listLongestSequence() {
        List<City> initialList = listAll();
        int[] idsByAsc = new int[initialList.size()];

        //create an array of ids in the ascending order
        for (int i=0; i < idsByAsc.length; i++) {
            idsByAsc[i] = initialList.get(i).getId();
        }

        //find the longest sequence
        List<Integer> optimalList = findLongestIncreasingSequence(idsByAsc);
        //convert List back to array for use in db queries
//        Integer[] optimalListAsArray = optimalList.toArray(new Integer[0]);
        return cityRepository.findAllById(optimalList);
    }

        //algorithm to determine longest sequence
        public static List<Integer> findLongestIncreasingSequence(int[] numbersToBeProcessed) {
            // `LIS[i]` stores the longest increasing subsequence of subarray
            // `numbersToBeProcessed[0…i]` that ends with `numbersToBeProcessed[i]`
            List<List<Integer>> LIS = new ArrayList<>();
            for (int i = 0; i < numbersToBeProcessed.length; i++) {
                LIS.add(new ArrayList<>());
            }

            // `LIS[0]` denotes the longest increasing subsequence ending at `numbersToBeProcessed[0]`
            LIS.get(0).add(numbersToBeProcessed[0]);

            // start from the second numbersToBeProcesseday element
            for (int i = 1; i < numbersToBeProcessed.length; i++)
            {
                // do for each element in subnumbersToBeProcesseday `numbersToBeProcessed[0…i-1]`
                for (int j = 0; j < i; j++)
                {
                    // find the longest increasing subsequence that ends with `numbersToBeProcessed[j]`
                    // where `numbersToBeProcessed[j]` is less than the current element `numbersToBeProcessed[i]`

                    if (numbersToBeProcessed[j] < numbersToBeProcessed[i] && LIS.get(j).size() > LIS.get(i).size()) {
                        LIS.set(i, new ArrayList<>(LIS.get(j)));
                    }
                }

                // include `numbersToBeProcessed[i]` in `LIS[i]`
                LIS.get(i).add(numbersToBeProcessed[i]);
            }

            // `j` will store an index of LIS
            int j = 0;
            for (int i = 0; i < numbersToBeProcessed.length; i++)
            {
                if (LIS.get(j).size() < LIS.get(i).size()) {
                    j = i;
                }
            }
            return LIS.get(j);
        }

}


