package com.example.eurovisionbackendexercise.controller;

import com.example.eurovisionbackendexercise.dao.CityRepository;
import com.example.eurovisionbackendexercise.entity.City;
import com.example.eurovisionbackendexercise.service.ServiceCity;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest(Controller.class)
class ControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CityRepository cityRepository;

    @MockBean
    private ServiceCity serviceCity;

    @Test
    void getCities() {

        City city1 = new City(1,"Alicante");
        City city2 = new City(2,"Madrid");
        City city3 = new City(3,"Barcelona");

        List<City> allCities = new ArrayList<>();
        allCities.add(city1);
        allCities.add(city2);
        allCities.add(city3);


        given(serviceCity.listAll()).willReturn(allCities);

    }

    @Test
    void getLongestSequenceCities() {

        City city1 = new City(1,"Alicante");
        City city2 = new City(2,"Madrid");
        City city3 = new City(3,"Barcelona");
        City city4 = new City(5,"BreakCity");

        List<City> allCities = new LinkedList<>();
        allCities.add(city1);
        allCities.add(city2);
        allCities.add(city4);
        allCities.add(city3);

        given(serviceCity.listLongestSequence()).willReturn(allCities);

    }

    @Test
    void getCitiesPaged() {
    }
}
