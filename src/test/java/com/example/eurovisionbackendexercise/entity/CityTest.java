package com.example.eurovisionbackendexercise.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CityTest {

    @Test
    public void testCityConstructor() {
        City testCity = new City(1,"Alicante");
        assertEquals(testCity.getId(), 1);
        assertEquals(testCity.getName(), "Alicante");
    }
}
